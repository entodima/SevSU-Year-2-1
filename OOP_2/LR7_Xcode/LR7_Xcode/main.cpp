//
//  main.cpp
//  LR7_Xcode
//
//  Created by Дмитрий Пархоменко on 16.11.2017.
//  Copyright © 2017 Дмитрий Пархоменко. All rights reserved.
//

#include <iostream>
using namespace std;

template <typename T>
void replaceArr(const size_t size, T arr[])
{
    for (size_t j=0; j<size; j+=2)
    {
        T temp = arr[j];
        arr[j] = arr[j+1];
        arr[j+1] = temp;
    }
}

template <typename T>
void out(const size_t size, T arr[]) { for (size_t i = 0; i < size; i++) cout << arr[i] << ' '; cout << endl;}

int main() {
    
    const size_t iSize = 6, cSize = 8;
    int i_arr[iSize] = {1,2,3,4,5,6};
    char c_arr[cSize] = {'a','b','c','d','e','f','g','h'};
    
    out(iSize, i_arr);
    out(cSize, c_arr);
    cout << endl;
    
    replaceArr(iSize, i_arr);
    replaceArr(cSize, c_arr);
    
    out(iSize, i_arr);
    out(cSize, c_arr);
    
    return 0;
}
