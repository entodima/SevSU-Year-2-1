#include "stdafx.h"
#include <iostream>
#include <string>
using namespace std;

struct STUDENT
{
	int nmb, yearB, yearA;
	string fName, lName;

	struct OCENKI							//вложенная структура
	{
		int math, phys, prog, hist;
	}OC;
};


void inStudent(STUDENT &log)				//функция ввода студентов и их оценок
{
	cout << "vvedite nomer studenta: ";
	cin >> log.nmb;
	cout << "vvedite imya studenta: ";
	cin >> log.fName;
	cout << "vvedite familiu studenta: ";
	cin >> log.lName;
	cout << "vvedite god rozhdeniya studenta: ";
	cin >> log.yearB;
	cout << "vvedite god postupleniya studenta: ";
	cin >> log.yearA;

	cout << "matematika: ";
	cin >> log.OC.math;
	cout << "fizika: ";
	cin >> log.OC.phys;
	cout << "programmirovaniye: ";
	cin >> log.OC.prog;
	cout << "istoriya: ";
	cin >> log.OC.hist;
}

void outStudent(STUDENT &log)				//функция печати студентов и их оценок
{
	cout << "nomer studenta: " << log.nmb << endl;
	cout << "imya: " << log.fName << endl;
	cout << "familiya: " << log.lName << endl;
	cout << "god rozhdeniya: " << log.yearB << endl;
	cout << "god postupleniya: " << log.yearA << endl;


	cout << "matematika: " << log.OC.math << endl;
	cout << "fizika: " << log.OC.phys << endl;
	cout << "programmirovaniye: " << log.OC.prog << endl;
	cout << "istoriya: " << log.OC.hist << endl;
}

void sort(STUDENT &log1, STUDENT &log2)		//функция вывода студентов с двойками
{
	int y1 = 0, y2 = 0;

	if (log1.OC.hist == 2 || log1.OC.math == 2 || log1.OC.phys == 2 || log1.OC.prog == 2)
	{
		y1 = 1;
	}
	else
	{
		if (log2.OC.hist == 2 || log2.OC.math == 2 || log2.OC.phys == 2 || log2.OC.prog == 2)
		{
			y2 = 1;

		}
	}

	if ((y1 == 0) && (y2 == 0))
	{
		cout << "studentov, poluchivshih hotya bi odnu dvoiku net";
	}
	else
	{
		if (y1 == 1)
		{
			cout << "studenty, poluchivshie dvoiki: " << endl;
			cout << log1.lName << endl;
			if (y2 == 1)
			{
				cout << log1.lName << endl;
				cout << log2.lName << endl;
			}

		}
		else
		{
			if (y2 == 1)
			{
				cout << "studenty, poluchivshie dvoiki: " << endl;
				cout << log2.lName << endl;
			}
		}
	}
}

void printMenu()
{
	cout << "0. Vyhod\n" << "1. Vvod dannyh\n" << "2. Vyvod dannyh\n" << "3. Sortirovka\n";
}


int main()
{
	STUDENT std1, std2;
	int a;
	do {
		printMenu();
		cin >> a;
		if (a == 1)
		{
			inStudent(std1);
			inStudent(std2);
		}
		else
		{
			if (a == 2)
			{
				outStudent(std1);
				outStudent(std2);
			}
			else
			{
				if (a == 3)
				{
					sort(std1, std2);
				}
				else cout << "Vvedite nomer menu\n";
			};
		};
		
	} while (a != 0);

	return 0;
}