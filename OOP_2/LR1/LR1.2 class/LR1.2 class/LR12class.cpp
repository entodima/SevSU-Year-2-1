#include "stdafx.h"
#include <iostream>
#include <string>
using namespace std;


class OCENKI 
{
private: int math, phys, prog, hist;
public:
	void inOcenki();		//���� ������ ���������
	void outOcenki();		//����� ������ ���������
	
	bool sortY();			//������ �� ��, ���� �� ������ � ��������
};

class STUDENT 
{
private:
	int nmb, yearB, yearA;
	string fName, lName;
public: 
	OCENKI OC;
	void inStudent();		//���� ������ � ��������
	void outStudent();		//����� ������ � ��������
	bool sortOut(int &b);	//����� ��������������� ������ � ������� ���������
	void outFamiliya();		//����� ������� (����������)

};

void OCENKI::inOcenki()			//���� ������ ���������
{
	cout << "matematika: ";
	cin >> math;
	cout << "fiziha: ";
	cin >> phys;
	cout << "programmirovanie: ";
	cin >> prog;
	cout << "istoriya: ";
	cin >> hist;
}

void OCENKI::outOcenki()		//����� ������ ���������
{
	cout << "matematika: " << math << endl;
	cout << "fizika: " << phys << endl;
	cout << "programmirovaniye: " << prog << endl;
	cout << "istoriya: " << hist << endl;
}

bool OCENKI::sortY()			//������ �� ��, ���� �� ������ � ��������
{
	bool y = 0;
	if (hist == 2 || math == 2 || phys == 2 || prog == 2)
	{
		y = 1;
	};
	return y;
}

void STUDENT::inStudent()		//���� ������ � ��������
{
	cout << "Vvedite nomer studenta: ";
	cin >> nmb;
	cout << "Vvedite imya studenta: ";
	cin >> fName;
	cout << "Vvedite familiyu studenta: ";
	cin >> lName;
	cout << "Vvedite god rozhdeniya studenta: ";
	cin >> yearB;
	cout << "Vvedite god postupleniya studenta: ";
	cin >> yearA;
}

void STUDENT::outStudent()		//����� ������ � ��������
{
	cout << "nomer studenta: " << nmb << endl;
	cout << "imya studenta: " << fName << endl;
	cout << "familiya studenta: " << lName << endl;
	cout << "god rozhdeniya studenta: " << yearB << endl;
	cout << "god postupleniya studenta: " << yearA << endl;
}

bool STUDENT::sortOut(int &b)	//����� ��������������� ������ � ������� ���������
{
	switch (b)
	{
	case 0:
		switch (OC.sortY())
		{
		case 0:
			b = 1;
			break;
		case 1:
			cout << "studenty, poluchivshie dvoiki: \n" << lName << endl;
			b = 2;
			break;
		}
		break;
	case 1:
		switch (OC.sortY())
		{
		case 0:
			cout << "studentov, poluchivshih dvoiki net" << endl;
			break;
		case 1:
			cout << "studenty, poluchivshie dvoiki: \n" << lName << endl;
			break;
		}

		break;
	case 2:
		switch (OC.sortY())
		{
		case 1:
			cout << lName << endl;
			break;
		}
		break;
	}

	return b;
}

void STUDENT:: outFamiliya()	//����� ������� (����������)
{
	cout << lName << endl;
}

int main()
{
	int a;
	int b = 0;
	OCENKI OC;
	STUDENT std1, std2;
	
	do
	{
		cout << "0. Vyhod\n" << "1. Vvod dannyh\n" << "2. Vyvod dannyh\n" << "3. Sortirovka\n";
		cin >> a;
		switch (a)
		{
		case 0: break;
		case 1: 
			std1.inStudent();
			std1.OC.inOcenki();
			cout << endl;
			std2.inStudent();
			std2.OC.inOcenki();
			cout << endl;
			break;
		case 2: 
			std1.outStudent();
			std1.OC.outOcenki();
			cout << endl;
			std2.outStudent();
			std2.OC.outOcenki();
			cout << endl;
			break;
		case 3: 
			std1.sortOut(b);
			std2.sortOut(b);
			cout << endl;
			break;
		}

	} while (a != 0);
	
    return 0;
}