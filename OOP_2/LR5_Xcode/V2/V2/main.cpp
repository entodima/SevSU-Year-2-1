#include <iostream>
using namespace std;
class Number;
class MatrixInteger {
    int matrix[3][3] = {
        {0,5,6},
        {0,3,9},
        {2,1,0}
    };
    friend void Op(MatrixInteger int1, Number nmb1);    //объявление дружественной функции
    
public:
    
    
    void out() {
        for (size_t i = 0; i<3; i++) {
            for (size_t j = 0; j<3; j++) {
                cout << matrix[i][j] << "   ";
            }
            cout << endl;
        }
    }
};

class Number {
    int n = 2;
    friend void Op(MatrixInteger int1, Number nmb1);    //объявление дружественной функции
    
};

void Op(MatrixInteger int1, Number nmb1) {             //определение дружественной функции
    for (size_t i = 0; i<3; i++) {
        for (size_t j = 0; j<3;j++) {
            int1.matrix[i][j] *= nmb1.n;
        }
    }
    
    
    int1.out();
    
    cout << endl;
}

int main() {
    
    MatrixInteger int1;
    int1.out();
    cout << endl;
    
    Number nmb1;
    cout << endl;
    
    Op(int1, nmb1);               //вызов дружественной функции
    
    return 0;
}

