//
//  main.cpp
//  LR2_Xcode
//
//  Created by Дмитрий Пархоменко on 09.11.2017.
//  Copyright © 2017 Дмитрий Пархоменко. All rights reserved.
//

#include <iostream>
#include <iomanip>          //для вывода огр кол знаков после запятой (cout << setprecision(3) << x;)

using namespace std;

int main()
{
/* CОЗДАНИЕ ПЕРЕМЕННЫХ */
    float **ptr_matrixL = new float*[5];
    float **ptr_matrixU = new float*[5];
    float *ptr_matrixB = new float[5];
    for (size_t i = 0; i<5; i++)
    {
        ptr_matrixL[i] = new float[5];
        ptr_matrixU[i] = new float[5];
    }
    
    float matrixL[5][5] =
    {
        {1, 0, 0, 0, 0},
        {0.55, 1, 0, 0, 0},
        {0.66, 0.958, 1, 0, 0},
        {0.27, 0.267, 0.642, 1, 0},
        {1, -0.267, -0.642, -0.853, 1}
    };
    float matrixU[5][5] =
    {
        {3.6, -2, -13.3, 0.4, 0.5},
        {0, 5.33, 11.9, -1.07, -0.5},
        {0, 0, -8.5, -10.3, 1.8},
        {0, 0, 0, 7.6, -2.4},
        {0, 0, 0, 0, -4.7}
    };
    string matrixB = {1, 2, -3, -1, 3};
    
    for (size_t i = 0; i<5; i++)
    {
        for (size_t j = 0; j<5; j++) {
            ptr_matrixL[i][j] = matrixL[i][j];
            ptr_matrixU[i][j] = matrixU[i][j];
        };
        ptr_matrixB[i] = matrixB[i];
    };
    
    /* конечные матрицы */
    float *ptr_matrixX = new float[5];
    float *ptr_matrixY = new float[5];
    
    
/* РАБОТА С МАТРИЦАМИ */
    
    for (size_t i=0; i<5; i++)  //L * y = b
    {
        float S = 0;
        for (size_t j = 0; j<i; j++) {
            S = S + ptr_matrixL[i][j] * ptr_matrixY[j];
        }
        ptr_matrixY[i] = ptr_matrixB[i] - S;
    }
    
    for (size_t i=0; i<5; i++)  //U * x = y
    {
        float S = 0;
        for (size_t j = 0; j<i; j++) {
            S = S + ptr_matrixU[i][j] * ptr_matrixX[j];
        }
        ptr_matrixX[i] = ptr_matrixY[i] / ptr_matrixU[i][i] - S;
    }
    
    cout << "y:" << endl;
    for (size_t i=0; i<5; i++)
    {
        cout << ptr_matrixY[i] << endl;
    }
    
    cout << endl << "x:" << endl;
    for (size_t i=0; i<5; i++)
    {
        cout << ptr_matrixX[i] << endl;
    }
    
/* УДАЛЕНИЕ ПЕРЕМЕННЫХ */
    for (size_t i = 0; i<5; i++)
    {
        delete[]ptr_matrixL[i];
        delete[]ptr_matrixU[i];
    }
    delete [] ptr_matrixL, ptr_matrixU, ptr_matrixB, ptr_matrixX, ptr_matrixY;
    
    return 0;
}
