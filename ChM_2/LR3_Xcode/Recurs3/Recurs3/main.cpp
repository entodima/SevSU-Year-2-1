//
//  main.cpp
//  Recurs3
//
//  Created by Дмитрий Пархоменко on 20.12.2017.
//  Copyright © 2017 Дмитрий Пархоменко. All rights reserved.
//

#include <iostream>
using namespace std;

double summL(int size,int coll, double **ptr_massL, double *ptr_massB)
{
    if (size < 1) return 1;
    
    double result = 0;
    for (int i = 0; i < size; i++) result+=ptr_massL[size][i]*ptr_massB[i];
    return result;
    
    //return summL(size ,coll-1, ptr_massL, ptr_massB) + ptr_massL[size][coll] * ptr_massB[size]; - рекурсивно
}

double findY(int size, double **ptr_massL, double *ptr_massB)
{
    if (size > 0)
    {
        cout << findY(size-1,ptr_massL,ptr_massB) << endl;
        return (*ptr_massB - summL(size,size, ptr_massL,ptr_massB));
    }
    double summ = 0;
    for (int i = 0; i< size; i++) summ+=ptr_massL[size][i];
    return (*ptr_massB - summL(size,size, ptr_massL,ptr_massB));
    
}

int main(int argc, const char * argv[])
{
    double massL[4][4] = {  {1,0,0,0},
        {2,1,0,0},
        {3,2,1,0},
        {4,3,2,1}};
    double massB[4] = {1,2,3,4};
    int size = 4;
    
    //Динамическая матрица L
    double **ptr_massL = new double*[size];
    for (size_t i = 0; i<size; i++) ptr_massL[i] = new double[size];
    for (size_t i=0; i<size; i++) for (size_t j=0; j<size; j++) ptr_massL[i][j] = massL[i][j];
    
    //Динамический вектор b
    double *ptr_massB = new double[size];
    for (size_t i=0; i<size; i++) ptr_massB[i] = massB[i];
    
    cout << "Y:\n" << findY(size-1,ptr_massL,ptr_massB) << endl;
    
    return 0;
}
